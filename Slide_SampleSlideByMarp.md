---
marp: true
paginate: true
theme: gaia
_class: lead
header: 
footer: [Typememo.com](https://typememo.com)
style: |
  section {
      background-color: #180614;
      color: #eaf4fc;
  }
  footer {
      font-size: 12px;
  }
---

# Marp Sample

Takeru Yamada

---

## Marp Info Source

![bg right fit](./../EyeCatch/Git.jpeg)

- [Marp GetStarted](https://marp.app/#get-started)
- [Marpit](https://marpit.marp.app/)
- [Marp Theme](https://github.com/marp-team/marp-core/tree/master/themes)
- [Image Syntax](https://marpit.marp.app/image-syntax) =>
- [和色大辞典](https://www.colordic.org/w)

---

## Third Slide

３次元空間におけるシュレディンガー方程式

$$
i\bar{h} \frac{\partial \Psi}{\partial t}
= - \frac{\bar{h}^{2}}{2m}
    \bigg \{
    \frac{\partial^{2} \Psi}{\partial x^{2}}
    + \frac{\partial^{2} \Psi}{\partial y^{2}}
    + \frac{\partial^{2} \Psi}{\partial z^{2}}
    \bigg \}
  + V(x, y, z) \Psi
$$

[Fate Stay Night Ⅲ 特報２生放送 @2019.12.19.Thu 21:00 - 22:00](https://abema.tv/channels/abema-anime/slots/CA5mxCTx9aWG7q?utm_medium=social&utm_source=twitter&utm_campaign=official_tw_anime)

---

## Texpad Info Source

* [Texpad MacOSX](https://www.texpad.com/mac)
* [Texpad Guide MacOSX](https://www.texpad.com/guide/osx)
* [Texpad Support MacOSX](https://www.texpad.com/support/macos)

---

## Python AstroPy

* [AstroPy](https://www.astropy.org/)
* [Python.org](https://www.python.org/)
* [Fits File Handling AstroPy](https://docs.astropy.org/en/stable/io/fits/)

[Introduction to IDL](http://www.faculty.virginia.edu/rwoclass/astr511/IDLguide.html)

---

## Images

![bg contain](./../EyeCatch/Git.jpeg)
![bg contain](./../EyeCatch/Git.jpeg)
