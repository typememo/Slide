# Slide

Marp Slide Store

My Marp template is shown below.

```markdown
---
marp: true
paginate: true
theme: gaia
_class: lead
header:
footer: [Typememo.com](https://typememo.com)
style: |
  section {
      background-color: #000b00;
      color: #eaf4fc;
  }
  footer {
      font-size: 12px;
  }
---
```
